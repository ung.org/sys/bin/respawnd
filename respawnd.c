/*
 * UNG's Not GNU
 *
 * Copyright (c) 2020 Jakob Kaivo <jkk@ung.org>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#define _XOPEN_SOURCE 700
#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <sys/wait.h>
#include <syslog.h>
#include <unistd.h>
#include <wordexp.h>

#ifndef DEFAULT_RESPAWN_COMMAND
#define DEFAULT_RESPAWN_COMMAND "/sys/bin/getty"
#endif

struct spawn {
	struct spawn *next;
	char *cmd;
	pid_t pid;
};

int main(int argc, char *argv[])
{
	struct spawn child = { NULL, DEFAULT_RESPAWN_COMMAND, 0 };
	struct spawn *head = &child;

	int c;
	while ((c = getopt(argc, argv, "")) != -1) {
		switch (c) {
		default:
			return 1;
		}
	}

	if (argc > optind) {
		fprintf(stderr, "respawnd: unexepected operands\n");
		return 1;
	}

	if (0 && getppid() != 1) {
		/* detach and reparent to init */
		return 0;
	}

	openlog("respawnd", LOG_PID | LOG_ODELAY | LOG_NOWAIT, LOG_DAEMON);
	for (;;) {
		for (struct spawn *p = head; p != NULL; p = p->next) {
			if (p->pid == 0) {
				p->pid = fork();
				if (p->pid == -1) {
					syslog(LOG_CRIT, "couldn't fork(): %s",
						strerror(errno));
					continue;
				}

				if (p->pid == 0) {
					/*
					wordexp_t we = { 0 };
					if (wordexp(p->cmd, &we, WRDE_NOCMD) != 0) {
						syslog(LOG_ERR, "bad expansion");
						return 1;
					}
					*/
					
					//execv(we.we_wordv[0], we.we_wordv);
					char *args[] = { p->cmd, NULL };
					execv(args[0], args);
					syslog(LOG_ERR, "couldn't exec %s: %s",
						p->cmd, strerror(errno));
					return 1;
				}
			}
		}
		
		int status = 0;
		pid_t child = wait(&status);
		for (struct spawn *p = head; p != NULL; p = p->next) {
			if (p->pid == child) {
				if (status != 0) {
					syslog(LOG_ERR, "child process '%s' exited with status %d, not restarting", p->cmd, status);
					p->pid = -1;
				} else {
					p->pid = 0;
				}
			}
		}
	}
	closelog();
}
